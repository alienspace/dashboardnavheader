import React, { Component } from 'react';
import {
  // Grid,
  Col,
} from 'react-bootstrap';
import { IndexLink, Link } from 'react-router';
// import { nextStorage } from '../../../util/nextStorage';
import { APP_CONFIG } from './../../../boot.config';
import { TOOLS } from './../../../util/tools.global';
import { Auth } from './../../../auth.core';
import firebase from 'firebase';

/** CUSTOM COMPONENTS **/
import { styles } from './NavHeader.style';
import { Logo } from './../Logo/Logo';
export class NavHeaderDashboard extends Component{
  constructor(props){
    super(props);
    this.state = {
      products : [],
      categories : [],

      currentUser: this.props.currentUser,
      navMobileOpened: this.props.navMobileOpened,
      navSidebar: this.props.sidebar,
      currentMenu: this.props.currentMenu,
      itemsNavDashboard: [
        {
          id: 10,
          title: { rendered: 'Apps' },
          slug: 'apps',
          icon: 'th',
          disabled: this.props.packages.DASHBOARD.appManager || false
        },
        {
          id: 0,
          title: { rendered: 'Produtos' },
          slug: 'products',
          icon: 'shopping-cart',
          disabled: this.props.packages.DASHBOARD.product || true
        },
        {
          id: 1,
          title: { rendered: 'Conteúdo' },
          slug: 'pages',
          icon: 'file-o',
          disabled: this.props.packages.DASHBOARD.page || true
        },
        {
          id: 2,
          title: { rendered: 'Categorias' },
          slug: 'categories',
          icon: 'thumb-tack',
          disabled: this.props.packages.DASHBOARD.category || true
        },
        {
          id: 3,
          title: { rendered: 'Usuários' },
          slug: 'users',
          icon: 'users',
          disabled: this.props.packages.DASHBOARD.users || false
        },
        {
          id: 4,
          title: { rendered: 'Configurações' },
          slug: 'settings',
          icon: 'cogs',
          disabled: this.props.packages.DASHBOARD.settings || true
        }
      ]
    }

     this.onClickMobile = this.onClickMobile.bind(this);
     this.renderLogo = this.renderLogo.bind(this);
     this.renderNavDashboard = this.renderNavDashboard.bind(this);
     this.renderSidebarDashboard = this.renderSidebarDashboard.bind(this);

    //  this.currentUser = this.currentUser.bind(this);
     this.onClickTargetBlank = TOOLS.onClickTargetBlank.bind(this);
     this.signOut = this.signOut.bind(this);
  }

  componentDidMount(){
  };//componentDidMount();

  onClickMobile(e) {
    if(e)
      e.preventDefault();
    let viewport = window.innerWidth;
    if(viewport <= 768)
      this.setState({ navMobileOpened: !this.state.navMobileOpened });
  };//onClickMobile

  classNavHeader(){
    if( this.state.navSidebar ){
      if( this.props.currentUser ){
        if(this.state.navMobileOpened)
          return 'navHeader navSidebar navMobileOpened no-padding';
        return 'navHeader navSidebar no-padding';
      }else{
        return 'navHeader navSidebar hidden no-padding';
      }
    }

    return 'navHeader navHeaderDashboard no-padding';
  };//classNavHeader

  renderLogo(){
    if( this.props.isMobile ){
      return (<Logo style={{width:'50%',margin:'10px auto'}} className="logoNavMobile" src={'/assets/images/logo_volare_bco.png'} dashboard />);
    }else{
      return (
        <Col xs={12} md={2}>
          <Logo style={{width:'100%'}} className="logoHeader" src={'/assets/images/logo_volare_bco.png'} dashboard />
        </Col>
      );
    }
  };//renderLogo

  signOut(e){
    e.preventDefault();
    // Sign out
    firebase.auth().signOut();
    TOOLS.loginVisitor();
  };//signOut

  renderNavDashboard(){
    // eslint-disable-next-line
    const HOME = () => {
      return (<li key={0} style={styles.MenuItemLI}><IndexLink to={{pathname:'/dashboard/'}} activeClassName="active"> <span>Home</span></IndexLink></li>);
    }

    if(!this.state.navSidebar){
      return (
        <Col xs={12} md={10} className={'navMenuContainer no-padding'}>
          <span className={'navSettingsIcon'} onClick={this.onClickMobile}></span>
          { this.props.currentUser && this.props.currentUser.email !== APP_CONFIG.MAIL_AUTH_VISITOR ?
            <ul style={{margin:'0 auto',padding:'0 3px'}}>
              {/*HOME()*/}
              {/*
                this.state.itemsNavDashboard.map((item) => {
                  if(this.state.currentMenu == '/'+item.slug){
                    return (<li key={item.id} style={styles.MenuItemLI}><Link className={'fa fa-hashtag'} to={{pathname:'/'+item.slug}} activeClassName="active"> <span>{item.title.rendered}</span></Link></li>);
                  }else{
                    return (<li key={item.id} style={styles.MenuItemLI}><Link to={{pathname:'/'+item.slug}} title={item.title.rendered} activeClassName="active"> <span>{item.title.rendered}</span></Link></li>);
                  }
                }) */
              }
              <li key={'user'}><Link className={'fa fa-user-circle-o'} to={'/dashboard/logout'} onClick={this.signOut}> <span>Admin - {this.props.currentUser.email}</span></Link></li>
              <li key={'shopping'}><Link className={'fa fa-shopping-bag label-info'} to={'/'} onClick={this.onClickTargetBlank} style={{padding:'5px 10px',fontSize:12}}> <span>Meu Site</span></Link></li>
              <li key={'exit'}><Link className={'fa fa-sign-out label-danger'} to={'/dashboard/logout'} style={{padding:'5px 10px',fontSize:12}} onClick={this.signOut}> <span>Sair</span></Link></li>
            </ul>
          :
            <Auth modeForm={'horizontal'} currentUser={this.props.currentUser} />
          }
        </Col>
      );
    }else{
      return (
        <Col xs={12} md={10} className={'navMenuContainer no-padding'}>
          { this.props.currentUser && this.props.currentUser.email !== APP_CONFIG.MAIL_AUTH_VISITOR ?
            this.renderSidebarDashboard()
          :
            <Auth modeForm={'horizontal'} currentUser={this.props.currentUser} />
          }
        </Col>
      );
    }
  };//renderNavDashboard();

  renderSidebarDashboard(){
    if( this.props.currentUser && this.props.currentUser.email !== APP_CONFIG.MAIL_AUTH_VISITOR ){
      const renderList = () => {
        return this.state.itemsNavDashboard.map((item) => {
          if(item.disabled){
            return (<li key={item.id}><Link className={'fa fa-'+item.icon} to={{pathname:'/dashboard/'+item.slug}} activeClassName="active"> <span>{item.title.rendered}</span></Link></li>);
          }else{
            return (<li key={item.id}><Link className={'disabled fa fa-'+item.icon} to={{pathname:'/dashboard/'+item.slug}} activeClassName="active"> <span>{item.title.rendered}</span></Link></li>);
          }
        });
      }
      return (
        <ul style={styles.MenuItem} className={'navSidebar'}>
          <span className={'navIcon'} onClick={this.onClickMobile}></span>
          <li key={0}><IndexLink className={'fa fa-home'} to={{pathname:'/dashboard/'}} activeClassName="active"> <span>Home</span></IndexLink></li>
        { renderList() }
        <li key={'exit'}><Link className={'fa fa-sign-out'} to={'/dashboard/logout'} style={{padding:'5px 20px',fontSize:12}} onClick={this.signOut}> <span>Sair</span></Link></li>
        </ul>
      );
    }
  };//renderSidebarDashboard();

  render(){
    let renderView = () => {
      if( !this.state.navSidebar ){
        return (
          <div>
            {this.renderLogo()}
            {this.renderNavDashboard()}
          </div>
        );
      }else{
        return (
          <Col xs={12} md={12} className={'no-padding'}>
            {this.renderNavDashboard()}
          </Col>

        );
      }
    }

    let classNavHeader = this.classNavHeader();

    let rend = () => {
      if(this.props.currentUser && this.props.currentUser.email !== APP_CONFIG.MAIL_AUTH_VISITOR){
        return (
          <Col xs={12} md={this.state.navSidebar ? 2 : 12} style={styles.MenuContainer} className={classNavHeader} onClick={this.onClickMobile}>
            {renderView()}
          </Col>
        );
      }
    }
    return (
      <div>
        {rend()}
      </div>
    );
  }
}
